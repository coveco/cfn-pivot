---
- name: cfn-pivot | have cfn-signal
  stat:
    path: '{{ cfn_signal_exe }}'
  register: signal_st

- name: cfn-pivot | signal that I am running
  command: >-
    {{ cfn_signal_exe }}
    --region=$AWS_DEFAULT_REGION
    --stack="$AWS_CFN_STACK_NAME"
    --resource=ClusterInit
  ignore_errors: yes
  when: signal_st.stat.executable

- name: cfn-pivot | have sentry-cli?
  stat:
    path: '{{ sentry_cli_exe }}'
  register: sentry_cli_stat

- name: cfn-pivot | set a "is development" flag for conditions
  set_fact:
    is_development: >-
      {{
        production_deploy_channel != (
          lookup("env", "DEPLOY_CHANNEL") | d(production_deploy_channel, True)
      ) }}
  vars:
    production_deploy_channel: 'openraven-deploy'

- name: cfn-pivot | run the main tasks in a rescue block
  block:
  - import_tasks: main_tasks.yml
  rescue:
  - name: cfn-pivot | package up the error
    # we have to compress this, otherwise the payload is too big for send-event below
    shell: gzip -9 | base64 -w0
    args:
      # we have to do that regex_replace dance because ansible's jinja2
      # evaluates **recursively** so referencing a task containing a failed
      # jinja2 expression will, here, *also* cause that same undefined variable error
      # and similar for the vars.ansible_failed_task because without the "vars."
      # it also causes recursive evaluation (try it for yourself!)
      # --
      # and that leading space is to keep ansible from coercing the "{}" string back into a dict
      # language=jinja2
      stdin: >-
        {{ " " + ({
             "failed_task": vars.ansible_failed_task | string,
             "failed_result": vars.ansible_failed_result,
           }
           | to_json
           | regex_replace("{[{%]", "<<")
           ) }}
    register: kaboom_b64

  - name: cfn-pivot | send Sentry error
    ignore_errors: yes
    when: sentry_cli_stat.stat.executable
    command: >-
      {{ sentry_cli_exe }} send-event
      -e 'groupId:{{ openraven_okta_group_id | d("") }}'
      -e 'deployChannel:{{ helm_s3_bucket | d("") }}'
      -u 'id:{{ openraven_okta_group_id | d("") }}'
      -r "$SENTRY_RELEASE"
      -m {{ ("failure: " + (ansible_failed_result.msg|d("MISSING MSG FIELD"))) | quote }}
    environment:
      SENTRY_DSN: '{{ sentry_dsn }}'
      # without the b64 sentry "filters" this value
      SENTRY_EXTRA: '{{ kaboom_b64.stdout }}'
      # the SENTRY_RELEASE is optionally populated by __pivot__.sh

  - name: cfn-pivot | send provision failure message
    # don't clog up the analytics for development deployments
    when: not is_development
    uri:
        method: POST
        url: '{{ api_base_url }}/account/activate/failure/{{ openraven_okta_group_id }}'
        body: '{{ payload | to_json }}'
        headers:
          content-type: application/json
    retries: 3
    vars:
      payload:
          "cluster-url": ""
          "deployChannel": "{{ helm_s3_bucket | d('') }}"

  - name: cfn-pivot | fail the run after sending failure outcome
    fail:
      msg: provisioning failed
