zk_cli() {
  zookeepercli --servers kafka-zookeeper.kafka.svc.cluster.local:2181 "$@"
}

zk_create_config_and_verify() {
  local zk_key="/config/application/openraven$1"
  local zk_value="$2"
  local zk_get_value
  if ! zk_cli -c exists  "$zk_key"; then
       zk_cli -c creater "$zk_key" "$zk_value"
  fi
  zk_cli -c set "$zk_key" "$zk_value"
  zk_get_value="$(zk_cli -c get "$zk_key")"
  if [[ "x${zk_get_value}" != "x${zk_value}" ]]; then
    return 1
  fi
  return 0
}

send_outcome_post() {
  local outcome="$1"
  if [[ "_${UPLOAD_LOGS_ON_FAILURE:-}" != '_yes' ]]; then
    return
  fi
  for _ in 1 2 3; do
    if curl -fsSH 'content-type: application/json' \
        --data-binary '{"cluster-url": "https://'"${OPENRAVEN_INGRESS_HOSTNAME}"'"}' \
        "{{ api_url }}/account/activate/${outcome}/${GROUP_ID}"
    then
      break
    fi
    sleep 2
  done
}

es_url='http://elasticsearch-es-http.elasticsearch.svc.cluster.local:9200'

mkdir /helmfiles
cd    /helmfiles || exit 1
now=$(date +%s)
wait_seconds=120
poll_timeout=$(( now + wait_seconds ))
while [[ $(date +%s) -lt $poll_timeout ]]; do
  if ! curl -fsSLo helmfile.yaml "$HELMFILE_REPO_URL"; then
    echo 'sleeping 5 for helmfile.yaml retry ...' >&2
    sleep 5
    continue
  fi
  break
done

helmfile_verb=sync
# if this is a restart, don't re-run sync or it'll try to create all new releases
existing_releases="$(helm ls --all --all-namespaces=true --output=json | jq -r '.|length')"
# using string equality should survive that helm-ls going toes up
if [[ "x${existing_releases}" != 'x0' ]]; then
  # using "apply" for subsequent runs no longer appears to be successful
  helmfile_verb=sync
fi

report_outcome=failure
for _ in 1 2 3; do
  if helmfile --log-level=debug ${helmfile_verb} --concurrency 1; then
    zk_create_config_and_verify /app/v1/assets-gremlin/database DemoAssetGraphNg
    zk_create_config_and_verify /app/v1/assets-gremlin/useDemoDb true

    zk_create_config_and_verify /app/v1/cloud-ingestion/dmap/bucket-name "$DMAP_S3_BUCKET"
    zk_create_config_and_verify /app/v1/cloud-ingestion/dmap/incoming-queue "$DMAP_SQS_URL"
    zk_create_config_and_verify /app/v1/cloud-ingestion/dmap/updateScanInterval 300000 # in ms (5 minutes)

    zk_create_config_and_verify /app/v1/dmap/scheduling/maxConcurrent 5
    zk_create_config_and_verify /app/v1/dmap/scheduling/enabled true

    zk_create_config_and_verify /app/v1/elastic-search-node-proxy/demoEsUrl "$es_url"

    # We create these because ESNC is going to come along and create them anyway...
    zk_create_config_and_verify /app/v1/elastic-search-node-proxy/demoEsUsername ""
    zk_create_config_and_verify /app/v1/elastic-search-node-proxy/demoEsPassword ""

    zk_create_config_and_verify /upgrades/etag ""
    zk_create_config_and_verify /upgrades/payload ""
    zk_create_config_and_verify /app/v1/gsuite-plugin ""
    zk_create_config_and_verify /app/v1/office365-plugin ""
    zk_create_config_and_verify /app/v1/cloud-ingestion/aws "$AWS_REGION_CSV"
    report_outcome=success
    break
  fi
done

# shellcheck disable=SC2016
get_non_running_count() {
  # yes, it's possible to do this without jq
  # but as we're in a jinja2 template the mustaches would be horrific
  kubectl get --all-namespaces=true -o json pods \
    | jq -r '
[
.items[]
| .status.phase as $p
| select($p != "Running" and $p != "Succeeded")
| $p
] | length'

}

if [[ $report_outcome == success ]]; then
  # stall until all Pods known to us are Running
  ok=0
  for _ in $(seq 1 60); do
    non_running=$(get_non_running_count || echo "1")
    if [[ $non_running -eq 0 ]]; then
      # ensure things are stable
      ok=$(( ok + 1 ))
      if [[ $ok -eq 3 ]]; then
        break
      fi
    fi
    sleep 10
  done

  # we want to delay until both ES is up, and also ESNC has loaded its data
  # before announcing "success" to the cluster owner
  expected_index=awsec2instance
  # it seems delays of up to 5 minutes just to have ES come alive are normal
  # and then it will take a little while for the ESNC job to run
  for _ in $(seq 1 60); do
    if ! curl -fs "$es_url/$expected_index/_count"; then
      sleep 10
      continue
    fi
    break
  done
fi

send_outcome_post $report_outcome
# since we have already sent the failure email, just give up
# (see the restart: OnFailure)
exit 0
