containerd_version=1.4.1
containerd_sha256=744ca14c62f5b2189c9fc22e9353508e49c85ab16038f3d74a852e6842e34fa3
# this one is manually calculated
containerd_bin_sha256=3cdc58d7837d63afe0835936220168b46604db53b2b164cb5dde01110b12282a
containerd_release_url=https://github.com/containerd/containerd/releases/download/v${containerd_version}/containerd-${containerd_version}-linux-amd64.tar.gz
if ! echo "$containerd_bin_sha256 /opt/bin/containerd" | sha256sum -c; then
  curl -fsSLo /tmp/containerd.tar.gz "$containerd_release_url"
  echo "$containerd_sha256 /tmp/containerd.tar.gz" | sha256sum -c
  # because the tar is "bin/containerd" and "bin/ctr"
  tar -xzf /tmp/containerd.tar.gz -C /opt
  rm /tmp/containerd.tar.gz
  systemctl restart containerd.service
fi

runc_bin=/opt/bin/runc
runc_version=1.0.0-rc91
runc_release_url=https://github.com/opencontainers/runc/releases/download/v${runc_version}/runc.amd64
# https://github.com/opencontainers/runc/releases/download/v${runc_version}/runc.sha256sum
runc_sha256=c1c30e250ba1af1bc513a37a85aef8c7824e9b8e76d10c933add58a72d84fce6
if ! echo "$runc_sha256  $runc_bin" | sha256sum -c; then
  curl -fsSLo "$runc_bin" "$runc_release_url"
  chmod 0755  "$runc_bin"
  echo "$runc_sha256  $runc_bin" | sha256sum -c
fi

crictl_version=v1.19.0
crictl_release_url="https://github.com/kubernetes-incubator/cri-tools/releases/download/${crictl_version}/crictl-${crictl_version}-linux-amd64.tar.gz"
# https://github.com/kubernetes-sigs/cri-tools/releases/tag/v1.19.0
crictl_sha256=87d8ef70b61f2fe3d8b4a48f6f712fd798c6e293ed3723c1e4bbb5052098f0ae
# crictl_bin=/opt/bin/crictl-$crictl_version
crictl_bin=/opt/bin/crictl
if [[ ! -x $crictl_bin ]]; then
  cd /tmp || exit 1
  curl -fSSLo crictl.tar.gz "$crictl_release_url"
  echo "$crictl_sha256  crictl.tar.gz" | sha256sum -c
  tar -xzf crictl.tar.gz
  rm       crictl.tar.gz
  mv -v crictl $crictl_bin
  ln -sf $crictl_bin /opt/bin/crictl
  cd /root || exit 1
fi

# https://github.com/kubernetes/release/raw/v0.4.0/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service
# https://github.com/kubernetes/release/raw/v0.4.0/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf
kubernetes_binaries_url="https://storage.googleapis.com/kubernetes-release/release/${K8S_VERSION}/bin/linux/amd64"
for b in kubeadm kubelet; do
  b_exe=/opt/bin/kubelet-${K8S_VERSION}
  if [[ ! -x     $b_exe ]]; then
    curl -fsSLo "$b_exe" "$kubernetes_binaries_url/$b"
    chmod 0755  "$b_exe"
    ln -sf      "$b_exe" /opt/bin/$b
  fi
done
