#! /usr/bin/env bash
set -euo pipefail

TRACE="${TRACE:-}"
[[ -n "$TRACE" ]] && set -x


# apologies for the name, but it's likely a consistency win
HELM_S3_BUCKET="${HELM_S3_BUCKET:-}"
if [[ -z "${HELM_S3_BUCKET}" ]]; then
    echo 'Not without HELM_S3_BUCKET' >&2
    exit 1
fi

curl -fsSLo etcdadm.zip \
      https://gitlab.com/openraven/open/etcdadm-build/-/jobs/506813685/artifacts/download
unzip -q etcdadm.zip
rm etcdadm.zip
mv etcdadm etcdadm_bin
mv etcdadm_bin/etcdadm .
rmdir etcdadm_bin

CI_COMMIT_SHA="${CI_COMMIT_SHA:-}"
if [[ -z "${CI_COMMIT_SHA}" ]]; then
    CI_COMMIT_SHA="$(git rev-parse HEAD)"
fi

tar_fn=cfn-pivot.tar
git archive --format tar --output cfn-pivot.tar "$CI_COMMIT_SHA"

aws s3 cp "$tar_fn" "s3://${HELM_S3_BUCKET}/$tar_fn"

if [[ -e etcdadm ]]; then
  echo "Publishing etcdadm also" >&2
  aws s3 cp etcdadm "s3://${HELM_S3_BUCKET}/bin/etcdadm"
  .gitlab/generate_manifest.py etcdadm $tar_fn
else
  .gitlab/generate_manifest.py $tar_fn
fi

aws s3 cp bootstrap_cfn_pivot_manifest.json "s3://${HELM_S3_BUCKET}/bootstrap_cfn_pivot_manifest.json"