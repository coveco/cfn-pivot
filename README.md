# What

Launched from [the CloudFormation stack](https://gitlab.com/openraven/open/aws-marketplace/blob/master/cloudformation/stack.yaml),
this ansible role is responsible for repeatedly updating the CloudFormation stack
as it gains more information along the way.

Then, it schedules a `helmfile0` Pod on the first control plane node, which begins
the in-cluster provisioning bits

# How to access the resulting Kubernetes cluster

You will need the `aws` command from [aws-cli](https://aws.amazon.com/cli/),
and a python interpreter containing `pyyaml` (which comes bundled with `ansible`,
in case you already have that, and the path to its python interpreter is
`$HOMEBREW_PREFIX/opt/ansible/libexec/bin/python`)

```sh
aws cloudformation describe-stack --stack-name $STACK_NAME | \
    python ./files/reconstruct_admin_conf.py -c
```

If you prefer to copy-paste the values, you can run that `reconstruct_admin_conf.py`
script without arguments and it will prompt for the two values using `input`.  

In either case, the resulting `KUBECONFIG` file will be written to the current
directory the the name printed by the script.

You may then set the `KUBECONFIG` environment variable to point to that file,
or merge it into your global `$HOME/.kube/config`, whichever you find more
convenient.
