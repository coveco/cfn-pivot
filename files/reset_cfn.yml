- hosts: localhost
  gather_facts: no
  vars:
    awscli_exe: /opt/ansible/bin/aws
    ansible_python_interpreter: /opt/ansible/bin/python
    # stack_json_file: ./stack.template
  tasks:
  - name: ensure etcd.service is stopped
    systemd:
      name: etcd
      state: stopped
      enabled: no
    ignore_errors: yes

  # we have to bring this down first, because kubeadm reset nukes the pki directory
  - name: ensure etcd is down
    command: /opt/bin/etcdadm reset --log-level debug --certs-dir /etc/kubernetes/pki/etcd

  - name: ensuring kubeadm is down
    command: /opt/bin/kubeadm reset --force --v=100
    ignore_errors: yes

  - name: bring containerd.service back up
    systemd:
      name: containerd.service
      state: started
      enabled: yes

  - name: ensuring kubelet.service is down
    systemd:
      name: kubelet.service
      state: stopped
      enabled: no
    ignore_errors: yes

  - name: revert etcd
    command: /opt/bin/etcdadm reset --log-level debug --certs-dir /etc/kubernetes/pki/etcd
    ignore_errors: yes
  - name: stop etcd
    systemd:
      name: etcd
      state: stopped
      enabled: no
    ignore_errors: yes

  - name: show what's up
    command: /bin/ls -l {{ item }}
    ignore_errors: yes
    with_items:
    - /etc/kubernetes
    - /etc/etcd
    - /var/lib/kubelet

  - name: cleaning up generated files
    file:
      path: '{{ item }}'
      state: absent
    with_fileglob:
    - /root/kubeadm-config.yaml
    - /root/calico.yaml
    - /root/calico.yaml.bak
    - /root/.kube/config
    - /root/worker.*
    - /root/control-plane.*
    - /root/helmfile0.yml
    - /root/tls.*

  - name: only do local changes if requested
    when: only_local|d(False, True)
    meta: end_play

  - name: consult the metadata endpoint about myself
    ec2_metadata_facts:

  - name: describe myself to obtain Tags
    ec2_instance_info:
      instance_ids:
      - '{{ ansible_ec2_instance_id }}'
      region: '{{ ansible_ec2_placement_region }}'
    register: my_instances

  - name: extract my CloudFormation stack name from my tags
    set_fact:
      aws_region: '{{ ansible_ec2_placement_region }}'
      my_stack_name: '{{ my_instances.instances[0].tags["aws:cloudformation:stack-name"] }}'

  - name: gather my CloudFormation stack info
    cloudformation_info:
      stack_name: '{{ my_stack_name }}'
      stack_template: yes
      region: '{{ aws_region }}'
    register: my_cf

  - name: apply any local patch to stack.json
    set_fact:
      my_stack: >-
        {{ my_cf.cloudformation[my_stack_name]
        | combine(stack_patch)
        }}
    vars:
      stack_patch: >-
        {{ {"stack_template": (" " + lookup("file", stack_json_file)) }
        if (stack_json_file is defined) else {}
        }}

  - name: rotate the stack back to its initial state
    import_tasks: ../tasks/change_cfn_parameters.yml
    vars:
      new_cfn_parameters:
        AdminConfB64: ''
        MasterPoolSize: "0"
        WorkerPoolSize: "0"
